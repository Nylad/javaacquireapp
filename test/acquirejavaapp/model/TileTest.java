package acquirejavaapp.model;

import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author nylad
 */
public class TileTest {

    public TileTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCompareToByCol() {
        //row 7, col 4
        Tile target = new Tile(7, 4);
        //row 1, col 5. instance should be higher, so we should get a positive result.
        Tile instance = new Tile(1, 5);
        int result = instance.compareTo(target);
        assertTrue(result > 0);
    }

    @Test
    public void testCompareToByRow() {
        //row 1, col 4
        Tile target = new Tile(1, 4);
        //row 4, col 4. instance should be higher, so we should get a positive result.
        Tile instance = new Tile(4, 4);
        int result = instance.compareTo(target);
        assertTrue(result > 0);
    }

    @Test
    public void testCompareToIdentical() {
        //row 1, col 4
        Tile target = new Tile(4, 4);
        //row 1, col 4. instance should be lower, so we should get a negative result.
        Tile instance = new Tile(4, 4);
        int result = instance.compareTo(target);
        assertTrue(result == 0);
    }

}
