package acquirejavaapp.model.players;

import acquirejavaapp.model.Chain;
import acquirejavaapp.model.Holdings;
import acquirejavaapp.model.Sale;
import acquirejavaapp.model.StockType;
import acquirejavaapp.model.TileCoords;
import java.util.List;
import java.util.Map;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author nylad
 */
public class PlayerTest {

    public PlayerTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//
//    /**
//     * Test of receiveTile method, of class Player.
//     */
//    @Test
//    public void testReceiveTile() {
//        System.out.println("receiveTile");
//        Tile tile = null;
//        Player instance = null;
//        instance.receiveTile(tile);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getMaxTradableHoldings method, of class Player.
     */
    @Test
    public void testGetMaxTradableHoldingsLimitedByBankShares() {
        System.out.println("getMaxTradableHoldings trading tower for continental");
        StockType targetType = StockType.TOWER;
        Holdings bankHoldings = new Holdings(18, 25, 25, 25, 25, 25, 2);
        Player instance = new AIRandomPlayer(0, "Joe", 0);
        instance.setHoldings(new Holdings(7, 0, 0, 0, 0, 0, 0));
        //We have 7 tower to burn, but the bank only has 2 continental to get.
        //should return 4.
        Integer expResult = 4;
        Integer result = instance.getMaxTradableHoldings(targetType, bankHoldings.get(StockType.CONTINENTAL));
        assertEquals(expResult, result);
    }

    /**
     * Test of getMaxTradableHoldings method, of class Player.
     */
    @Test
    public void testGetMaxTradableHoldings() {
        System.out.println("getMaxTradableHoldings 2");
        StockType targetType = StockType.TOWER;
        Holdings bankHoldings = new Holdings(18, 25, 25, 25, 25, 25, 7);
        Player instance = new AIRandomPlayer(0, "Joe", 0);
        instance.setHoldings(new Holdings(5, 0, 0, 0, 0, 0, 0));
        //We have 8 tower to burn, but the bank only has 2 continental to get.
        //should return 4.
        Integer expResult = 4;
        Integer result = instance.getMaxTradableHoldings(targetType, bankHoldings.get(StockType.CONTINENTAL));
        assertEquals(expResult, result);
    }

//    /**
//     * Test of releaseTile method, of class Player.
//     */
//    @Test
//    public void testReleaseTile() {
//        System.out.println("releaseTile");
//        Tile tile = null;
//        Player instance = null;
//        instance.releaseTile(tile);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    public class PlayerImpl extends Player {

        public PlayerImpl() {
            super(null, "", null);
        }

        @Override
        public TileCoords selectTile() {
            return null;
        }

        @Override
        public Sale buyStocks(Holdings availableStocks, Map<StockType, Chain> activeChains) {
            return null;
        }

        @Override
        public StockType chooseNewChainType(List<StockType> availableChains, Holdings holdingsRemaining) {
            return null;
        }

        @Override
        public StockType chooseWinningChain(List<StockType> tiedChains) {
            return null;
        }

        @Override
        public Integer promptTrade(StockType targetType, StockType bidderType, Integer bidderBankShares) {
            return null;
        }

        @Override
        public Integer promptSell(StockType type, Integer price) {
            return null;
        }

        @Override
        public boolean isHuman() {
            return false;
        }
    }

}
