package acquirejavaapp;

import acquirejavaapp.model.Holdings;
import acquirejavaapp.model.StockType;
import acquirejavaapp.model.bag.Bag;
import acquirejavaapp.model.bag.PredictableBag;
import acquirejavaapp.model.players.AIRandomPlayer;
import acquirejavaapp.model.players.Player;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author nylad
 */
public class GameTest {

    public GameTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//    /**
//     * Test of play method, of class Game.
//     */
//    @Test
//    public void testPlay() {
//        System.out.println("play");
//        Game instance = null;
//        instance.play();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of validatePurchasesMadeByPlayer method, of class Game.
//     */
//    @Test
//    public void testValidatePurchasesMadeByPlayer() {
//        System.out.println("validatePurchasesMadeByPlayer");
//        Player player = null;
//        Holdings boardHoldings = null;
//        Map<StockType, Chain> activeChains = null;
//        Game instance = null;
//        Sale expResult = null;
//        Sale result = instance.validatePurchasesMadeByPlayer(player, boardHoldings, activeChains);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of validateTradeFromPlayer method, of class Game.
//     */
//    @Test
//    public void testValidateTradeFromPlayer() {
//        System.out.println("validateTradeFromPlayer");
//        Player player = null;
//        Payout payout = null;
//        Game instance = null;
//        Integer expResult = null;
//        Integer result = instance.validateTradeFromPlayer(player, payout);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of splitPlayers method, of class Game.
     */
    @Test
    public void testSplitPlayers() {
        System.out.println("splitPlayers");
        StockType type = StockType.CONTINENTAL;
        List<Player> players = new ArrayList<>();
        AIRandomPlayer joe = new AIRandomPlayer(0, "Joe", 0, 6000, new Holdings(0, 0, 0, 0, 0, 0, 2));
        AIRandomPlayer alice = new AIRandomPlayer(0, "Bill", 0, 6000, new Holdings(0, 0, 0, 0, 0, 0, 2));
        AIRandomPlayer sue = new AIRandomPlayer(0, "Sue", 0, 6000, new Holdings(0, 0, 0, 0, 0, 0, 2));
        AIRandomPlayer bob = new AIRandomPlayer(0, "Bob", 0, 6000, new Holdings(0, 0, 0, 0, 0, 0, 1));
        AIRandomPlayer mary = new AIRandomPlayer(0, "Jane", 0, 6000, new Holdings());
        players.add(joe);
        players.add(alice);
        players.add(sue);
        players.add(bob);
        players.add(mary);

        Game instance = new Game(players);

        List<Player> result = instance.splitPlayers(type, players);
        Assert.assertTrue(result.containsAll(Arrays.asList(joe, alice, sue)));
        Assert.assertFalse(result.contains(bob));
        Assert.assertFalse(result.contains(mary));
    }

    @Test
    public void testDivideAndRoundUpSplitThreeWays() {
        Bag bag = new PredictableBag();
        System.out.println("divideAndRoundUp 3 way split");
        Game instance = new Game(new ArrayList<>());
        Integer amountToSplit = 2500;
        Integer splitCount = 3;
        Integer expResult = 900;
        Integer result = instance.divideAndRoundUp(amountToSplit, splitCount);
        assertEquals(expResult, result);
    }

    @Test
    public void testDivideAndRoundUpSplitOneWay() {
        Bag bag = new PredictableBag();
        System.out.println("divideAndRoundUp one way split");
        Game instance = new Game(new ArrayList<>());
        Integer amountToSplit = 2500;
        Integer splitCount = 1;
        Integer expResult = 2500;
        Integer result = instance.divideAndRoundUp(amountToSplit, splitCount);
        assertEquals(expResult, result);
    }

    @Test
    public void testDivideAndRoundUpSplitFiveWays() {
        Bag bag = new PredictableBag();
        System.out.println("divideAndRoundUp five way split");
        Game instance = new Game(new ArrayList<>());
        Integer amountToSplit = 2100;
        Integer splitCount = 5;
        Integer expResult = 500;
        Integer result = instance.divideAndRoundUp(amountToSplit, splitCount);
        assertEquals(expResult, result);
    }

}
