package acquirejavaapp;

import static acquirejavaapp.ValidationUtils.validateSaleFromPlayer;
import static acquirejavaapp.ValidationUtils.validateTradeFromPlayer;
import acquirejavaapp.model.Board;
import acquirejavaapp.model.Chain;
import acquirejavaapp.model.Payout;
import acquirejavaapp.model.StockType;
import acquirejavaapp.model.TileCoords;
import acquirejavaapp.model.players.Player;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;

public class Game {

    @Expose
    private final List<Player> players;
    @Expose
    private final Board board;
    private Player activePlayer;

    public Game(List<Player> players) {
        this.players = players;
        this.board = new Board();
    }

    public Game(String gameStateJson) {
        Gson gson = new Gson();
        Game game = gson.fromJson(gameStateJson, getClass());
        this.activePlayer = game.getActivePlayer();
        this.board = game.getBoard();
        this.players = game.getPlayers();
    }

    public Player rotateTurn() {
        activePlayer = players.get((players.indexOf(activePlayer) + 1) % (players.size()));
        delay(2);
        return activePlayer;
    }

    private void delay(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        }
        catch (InterruptedException ex) {
            System.out.println("interrupted sleep cycle!");
        }
    }

    public String serializeGame() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }

    public List<Payout> generateFinalPayouts(Map<StockType, Chain> activeChains) {
        System.out.println("Game Over!");
        System.out.println("Generating Final Payouts...");
        List<Payout> finalPayouts = new ArrayList<>();
        for (Map.Entry<StockType, Chain> entry : activeChains.entrySet()) {
            StockType key = entry.getKey();
            Chain value = entry.getValue();
            if (value != null) {
                finalPayouts.add(new Payout(key, null, value.getPrice()));
            }
        }
        return finalPayouts;
    }

    protected void printScores() {
        System.out.println("Leftover Holdings:");
        String out = "";
        for (Player player : players) {
            String innerResults = "";
            for (Map.Entry<StockType, Integer> entry : player.getHoldings().entrySet()) {
                Integer value = entry.getValue();
                if (value > 0) {
                    innerResults += "\t" + entry.getValue() + " " + entry.getKey() + "\n";
                }
            }
            if (!innerResults.isEmpty()) {
                out += player.getName() + " still had:\n" + innerResults;
            }
        }
        delay(2);
        if (!out.isEmpty()) {
            System.out.println(out);
        }
        System.out.println(board.printBoard());
        System.out.println("\n\n\nFINAL SCORES\n");
        List<Player> sortedPlayers = players.stream()
                .sorted((p2, p1) -> Integer.compare(p1.getMoney(), p2.getMoney()))
                .collect(toList());

        for (Player player : sortedPlayers) {
            System.out.println(player.getName() + ": " + player.getMoney());
        }
        delay(2);
        System.out.println("Thanks for playing!");
    }

    protected boolean canBuy(Player player) {
        List<Chain> availableStocks
                = board.getActiveChains()
                        .entrySet()
                        .stream()
                        .map(entry -> entry.getValue())
                        .filter(chain -> chain.getPrice() < player.getMoney())
                        .collect(toList());

        return !availableStocks.isEmpty();
    }

    protected Boolean endConditionMet() {
        return !board.getActiveChains().isEmpty() && (board.getActiveChains()
                .entrySet()
                .stream()
                .anyMatch(entry -> entry.getValue().size() > 40)
                                                      || board.getActiveChains()
                        .entrySet()
                        .stream()
                        .allMatch(entry -> entry.getValue().size() > 10));
    }

    public void getPlayerTiles() {
        for (Player player : players) {
            System.out.println("Drawing tiles for " + player.getName());
            for (int i = 0; i < 6; i++) {
                player.receiveTile(board.getTileFromBag());
            }
        }
    }

    public void placeInitiativeTiles() {
        Integer bestInitiativePlayerNum = -1;
        TileCoords bestInitiativeTile = null;
        for (Player player : players) {
            System.out.println("drawing initiative tile for " + player.getName());
            TileCoords tileFromBag = board.getTileFromBag();
            //if this is the first, it's the best. If it has a lower-indexed tile, it's better.
            if (bestInitiativeTile == null || tileFromBag.compareTo(bestInitiativeTile) < 0) {
                bestInitiativeTile = tileFromBag;
                bestInitiativePlayerNum = player.getPlayerNum();
            }
            board.placeInitiativeTile(player, tileFromBag);
            System.out.println(tileFromBag.toString());
        }
        activePlayer = players.get(bestInitiativePlayerNum);
        System.out.println("Player " + activePlayer.getName()
                           + " got the lowest tile (" + bestInitiativeTile + ") and will go first.");
    }

    protected void handlePayouts(List<Payout> payouts) {
        for (Payout payout : payouts) {
            if (payout.getBidderType() != null) {
                System.out.println("Merger!");
                System.out.println(payout.getBidderType() + " takes over " + payout.getTargetType());
            }
            else {
                System.out.println("Final Payouts for " + payout.getTargetType());
            }
            StockType targetType = payout.getTargetType();
            //get the players who have this stock, sorted by ownership.
            List<Player> tempPlayerList = players.stream().filter(p -> p.getHoldings().get(targetType) > 0)
                    .sorted((p2, p1) -> Integer.compare(
                    p1.getHoldings().get(targetType),
                    p2.getHoldings().get(targetType)))
                    .collect(toList());
            dispenseBonuses(tempPlayerList, payout);
            System.out.println("In order of stock ownership:");
            for (Player player : tempPlayerList) {
                /* payouts with no bidder type are endgame payouts. For these
                 * the player is forced to sell. */
                if (payout.getBidderType() == null) {
                    buyStockFromPlayer(player.getHoldings().get(targetType), player, payout);
                    continue;
                }
                if (board.getHoldings().get(payout.getBidderType()) != 0 && player.getHoldings().get(targetType) > 1) {
                    Integer bidderTradeCount = validateTradeFromPlayer(player, payout, board);
                    board.tradeStockWithPlayer(payout.getBidderType(), payout.getTargetType(), bidderTradeCount, player);
                    System.out.println("\t" + player.getName() + " traded " + bidderTradeCount * 2 + " " + targetType
                                       + " for " + bidderTradeCount + " " + payout.getBidderType() + ".");
                }
                if (player.getHoldings().get(targetType) > 0) {
                    buyStockFromPlayer(validateSaleFromPlayer(player, payout, board), player, payout);
                }
            }
            delay(3);
        }
    }

    private void buyStockFromPlayer(Integer sellCount, Player player, Payout payout) {
        board.buyStockFromPlayer(payout.getTargetType(), sellCount, player, payout.getPrice());
        System.out.println("\t" + player.getName() + " sold " + sellCount + " " + payout.getTargetType() + " for " + payout.getPrice() * sellCount + ".");
    }

    private void dispenseBonuses(List<Player> players, Payout payout) {
        Integer primary = 10 * payout.getPrice();
        Integer secondary = 5 * payout.getPrice();
        Integer both = primary + secondary;

        if (players.size() == 1) {
            //primary = 10 * price, secondary = 5 * price
            board.giveBonusToPlayer(players.get(0), both);
            System.out.println("Primary and Secondary: " + players.get(0).getName() + " - " + both);
        }
        else if (players.isEmpty()) {
            System.out.println("no one is holding this stock how did this happen this shouldn't be possible.");
        }
        //if top two are equal in holdings, sum and split the primary and secondary bonuses
        //between all tied players, rounding up to the nearest hundred if necessary.
        else if (players.get(0).getHoldings().get(payout.getTargetType())
                .equals(players.get(1).getHoldings().get(payout.getTargetType()))) {
            List<Player> splitPlayers = splitPlayers(payout.getTargetType(), players);
            Integer splitValue = divideAndRoundUp(both, splitPlayers.size());

            System.out.println("Primary and Secondary SPLIT: ");
            for (Player player : splitPlayers) {
                board.giveBonusToPlayer(player, splitValue);
                System.out.println("\t" + player.getName() + " receives " + splitValue);
            }
        }
        else {
            board.giveBonusToPlayer(players.get(0), primary);
            System.out.println("Primary: " + players.get(0).getName() + " receives " + primary);
            List<Player> tempPlayers = new ArrayList<>();
            tempPlayers.addAll(players);
            tempPlayers.remove(players.get(0));
            //since people might still be tied for second
            List<Player> splitPlayers = splitPlayers(payout.getTargetType(), tempPlayers);
            Integer splitValue = divideAndRoundUp(secondary, splitPlayers.size());
            if (splitPlayers.size() == 1) {
                board.giveBonusToPlayer(splitPlayers.get(0), splitValue);
                System.out.println("Secondary: " + splitPlayers.get(0).getName() + " receives " + splitValue);
            }
            else {
                System.out.println("Secondary SPLIT:");
                for (Player player : splitPlayers) {
                    board.giveBonusToPlayer(player, splitValue);
                    System.out.println("\t" + player.getName() + " receives " + splitValue);
                }
            }
        }
    }

    protected List<Player> splitPlayers(StockType type, List<Player> players) {
        List<Player> tiedPlayers = new ArrayList<>();
        Integer tiedAmount = players.get(0).getHoldings().get(type);
        players.stream().filter((player) -> (player.getHoldings().get(type).equals(tiedAmount))).forEachOrdered((player) -> {
            tiedPlayers.add(player);
        });
        return tiedPlayers;
    }

    protected Integer divideAndRoundUp(Integer amountToSplit, Integer splitCount) {
        Integer splitValue = amountToSplit / splitCount;
        //integer division off the leftovers, get it back to the right value, and give the "rounded up" value.
        splitValue = (splitValue % 100 == 0) ? splitValue : ((splitValue / 100) * 100) + 100;
        return splitValue;
    }

    public Board getBoard() {
        return board;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    private List<Player> getPlayers() {
        return players;
    }

}
