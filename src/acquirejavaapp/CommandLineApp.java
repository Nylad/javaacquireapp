package acquirejavaapp;

import acquirejavaapp.model.players.AIRandomPlayer;
import acquirejavaapp.model.players.HumanTextPlayer;
import acquirejavaapp.model.players.Player;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class CommandLineApp {

    static List<String> aiPlayerNames = new ArrayList<>(Arrays.asList("Antonio[AI]", "Hadrian[AI]", "Julius[AI]", "Marcus[AI]", "Tiberius[AI]", "Justin[AI]"));
    private static final Integer GAME_ID = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Welcome to Command Line Acquire!\n");
        Integer playerCount;
        List<String> playerNames = new ArrayList<>();
        if (args.length > 0) {
            handleArgs(args, playerNames);
        }
        else {
            playerCount = getPlayerCount(scan);
            playerNames = getPlayerNames(playerCount, scan);
        }
        System.out.println("\nPlayers are:");
        for (String name : playerNames) {
            System.out.println("\t" + name);
        }
        List<Player> players = initializePlayers(playerNames, scan);
        Game game = new Game(players);
        GameService gameService = new GameService();
        gameService.startGame(game);
    }

    private static void handleArgs(String[] args, List<String> playerNames) throws NumberFormatException {
        Integer playerCount;
        playerCount = Integer.parseInt(args[0]);
        for (int i = 0; i < playerCount; i++) {
            playerNames.add(args[i + 1]);
        }
    }

    private static Integer getPlayerCount(Scanner scan) {
        Integer playerCount = 0;
        while (playerCount > 6 || playerCount < 2) {
            System.out.print("How many players will be playing today?\n"
                             + "Players (2-6):");
            playerCount = scan.nextInt();
        }
        return playerCount;
    }

    private static List<String> getPlayerNames(Integer playerCount, Scanner scan) {
        String[] playerNames = new String[playerCount];
        System.out.println("Great, " + playerCount + " players.\n"
                           + "What are their names? "
                           + "(One name per line. Leave blank for a computer player, "
                           + "or append the name with [AI])");
        scan.nextLine(); //skip over the empty line we already got.
        for (int i = 0; i < playerCount; i++) {
            System.out.print("Player " + (i + 1) + ":");
            playerNames[i] = scan.nextLine();
            if (playerNames[i].trim().equalsIgnoreCase("")) {
                playerNames[i] = getAIPlayerName();
            }
        }
        return Arrays.asList(playerNames);
    }

    private static String getAIPlayerName() {
        Random rand = new Random();
        int index = rand.nextInt(aiPlayerNames.size());
        return aiPlayerNames.remove(index);
    }

    /**
     *
     * @param names names of all players
     * @param scan a scanner to assign to any text input players
     * @return a list of all players
     */
    private static List<Player> initializePlayers(List<String> names, Scanner scan) {
        List<Player> players = new ArrayList<>();
        for (int i = 0; i < names.size(); i++) {
            String name = names.get(i);
            if (name.toUpperCase().endsWith("[AI]")) {
                players.add(new AIRandomPlayer(GAME_ID, name.replace("[AI]", ""), i));
            }
            else {
                players.add(new HumanTextPlayer(GAME_ID, name, i, scan));
            }
        }
        return players;
    }

}
