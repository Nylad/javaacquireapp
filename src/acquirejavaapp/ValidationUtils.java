package acquirejavaapp;

import acquirejavaapp.model.Board;
import acquirejavaapp.model.Payout;
import acquirejavaapp.model.Sale;
import acquirejavaapp.model.TileCoords;
import acquirejavaapp.model.players.Player;

/**
 *
 * @author nylad
 */
public class ValidationUtils {

    private ValidationUtils() {
    }

    public static Sale validatePurchasesMadeByPlayer(Player player, Board board) {
        Sale sale = null;
        while (sale == null) {
            sale = player.buyStocks(board.getHoldings(), board.getActiveChains());

            if (sale == null) {
                //continue
            }
            else if (!board.getHoldings().hasAllHoldings(sale.getPurchases())) {
                System.out.println("there are not enough shares left to make that purchase.");
                sale = null;
            }
            else if (player.getMoney() < sale.getTotalPrice()) {
                System.out.println("That purchase costs " + sale.getTotalPrice() + ". You only have " + player.getMoney());
                sale = null;
            }
        }
        if (player.isHuman()) {
            System.out.println(board.printBoard());
        }
        System.out.println(player.getName() + " bought " + sale.toString());
        return sale;
    }

    public static TileCoords validateTileFromPlayer(Player player, Board board) {
        TileCoords tile = null;

        /**
         * List<Tile> confiscatedTiles = new ArrayList<>(); TODO: could just
         * hold on to unplayable tiles until the player chooses a good one, then
         * give back the others. it's technically possible to only have tiles
         * that would start a chain. If they can't start a chain and the
         * confiscatedTiles list gets to size 6, we should give them a new tile
         * for this turn, and give them back their old ones for next turn
         * (because they might want to start a chain). Very edge casey though.
         */
        while (tile == null) {
            tile = player.selectTile();
            if (tile == null) {
                //continue
            }
            else if (!player.getTiles().contains(tile)) {
                System.out.println("You do not have tile " + tile + ". Please play a different tile.");
                tile = null;
            }
            //if we have no stocks available, you can't play a tile that would start a new chain.
            else if (board.getInactiveStocks().isEmpty()) {
                if (board.wouldStartNewChain(tile)) {
                    System.out.println(tile + " would start a new chain, and there are no stock types available. Please play a different tile.");
                    tile = null;
                }
            }
            else if (board.wouldMergeSafeChains(tile)) {
                System.out.println(tile + " is between safe chains, so it cannot be played. Discarding.");
                //TODO: This is some logic and changing state in a utils class.
                //Not something I would expect. Redesign so this doesn't happen.
                board.discardTile(tile);
                player.releaseTile(tile);
                player.receiveTile(board.getTileFromBag());
                System.out.println(board.printBoard());
                tile = null;
            }
        }
        return tile;
    }

    //TODO: actually do this
    /**
     *
     * @param player
     * @param payout
     * @param board
     * @return the number of bidder type stock the player gets
     */
    public static Integer validateTradeFromPlayer(Player player, Payout payout, Board board) {
        return player.promptTrade(payout.getTargetType(), payout.getBidderType(), board.getHoldings().get(payout.getBidderType()));
    }

    //TODO: actually do this
    public static Integer validateSaleFromPlayer(Player player, Payout payout, Board board) {

        return player.promptSell(payout.getTargetType(), payout.getPrice());
    }
}
