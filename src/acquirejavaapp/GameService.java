package acquirejavaapp;

import static acquirejavaapp.ValidationUtils.validatePurchasesMadeByPlayer;
import static acquirejavaapp.ValidationUtils.validateTileFromPlayer;
import acquirejavaapp.model.Payout;
import acquirejavaapp.model.Sale;
import acquirejavaapp.model.TileCoords;
import java.util.List;

public class GameService {

    public void startGame(Game game) {
        System.out.println("Beginning game!");
        game.placeInitiativeTiles();
        game.getPlayerTiles();
        do {
            playTurn(game);
        }
        while (!game.endConditionMet());
        game.handlePayouts(game.generateFinalPayouts(game.getBoard().getActiveChains()));
        game.printScores();
    }

    private void playTurn(Game game) {
        if (game.endConditionMet()) {
            game.handlePayouts(game.generateFinalPayouts(game.getBoard().getActiveChains()));
            game.printScores();
        }
        System.out.println(game.getActivePlayer().getName() + ", it is your turn.");
        System.out.println(game.getBoard().printPlayerStatus(game.getActivePlayer()));
        TileCoords tile = validateTileFromPlayer(game.getActivePlayer(), game.getBoard());
        List<Payout> payouts = game.getBoard().placeActiveTile(game.getActivePlayer(), tile);
        System.out.println(game.getActivePlayer().getName() + " played " + tile);
        System.out.println(game.getBoard().printBoard());
        game.handlePayouts(payouts);
        if (game.canBuy(game.getActivePlayer())) {
            Sale sale = validatePurchasesMadeByPlayer(game.getActivePlayer(), game.getBoard());
            game.getBoard().sellStocksToPlayer(sale, game.getActivePlayer());
        }
        else {
            //make our output consistent
            System.out.println("\n\n\n\n");
        }
        game.getActivePlayer().receiveTile(game.getBoard().getTileFromBag());
        //System.out.println(game.serializeGame());
        game.rotateTurn();

    }
}
