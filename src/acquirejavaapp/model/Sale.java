package acquirejavaapp.model;

public class Sale {

    private final Holdings purchases;
    private final Integer totalPrice;

    public Sale(Holdings purchases, Integer totalPrice) {
        this.purchases = purchases;
        this.totalPrice = totalPrice;
    }

    public Holdings getPurchases() {
        return purchases;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    @Override
    public String toString() {
        if (purchases.isEmpty()) {
            return "nothing\n\n\n\n";
        }
        String out = "\n";
        Integer typeCount = 0;
        for (StockType type : StockType.values()) {
            if (purchases.get(type) > 0) {
                out += "\t\t" + purchases.get(type) + " " + type + "\n";
                typeCount++;
            }
        }
        out += "\t\t\tfor " + totalPrice;
        //makes our output be the same number of lines every time.
        while (typeCount < 3) {
            out += "\n";
            typeCount++;
        }
        return out;
    }

}
