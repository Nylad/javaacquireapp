package acquirejavaapp.model.players;

import acquirejavaapp.model.Chain;
import acquirejavaapp.model.Holdings;
import acquirejavaapp.model.Sale;
import acquirejavaapp.model.StockType;
import acquirejavaapp.model.TileCoords;
import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Player {

    @Expose
    private Integer gameId;
    @Expose
    protected String name;
    //private int userId; this will only apply for human players.
    @Expose
    private Integer playerNum; //for turn order
    @Expose
    protected Integer money;
    @Expose
    protected Holdings holdings;
    @Expose
    protected List<TileCoords> tiles;

    public Player(Integer gameId, String name, Integer playerNum) {
        this(gameId, name, playerNum, 6000, new Holdings());
    }

    public Player(Integer gameId, String name, Integer playerNum, Integer money, Holdings holdings) {
        this.gameId = gameId;
        this.name = name;
        this.playerNum = playerNum;
        this.money = money;
        this.holdings = holdings;
        this.tiles = new ArrayList<>();
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPlayerNum() {
        return playerNum;
    }

    public void setPlayerNum(Integer playerNum) {
        this.playerNum = playerNum;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Holdings getHoldings() {
        return holdings;
    }

    public void setHoldings(Holdings holdings) {
        this.holdings = holdings;
    }

    public List<TileCoords> getTiles() {
        return tiles;
    }

    public void setTiles(List<TileCoords> tiles) {
        this.tiles = tiles;
    }

    public void receiveTile(TileCoords tile) {
        if (tiles.size() == 6) {
            System.err.println("player " + name + " already has 6 tiles");
            return;
        }
        tiles.add(tile);
    }

    public String printTiles() {
        String out = "+-----+-----+-----+-----+-----+-----+";
        out += "\n|";
        //String out = "\n ------------------------\n|";
        for (int i = 0; i < tiles.size(); i++) {
            out += tiles.get(i).toString() + "|";
        }
        out += "\n+-----+-----+-----+-----+-----+-----+";
        return out;
    }

    public String printHoldings() {
        return (isHuman()) ? ("Your holdings:" + holdings.printHoldings()) : (name + " has:\n" + holdings.printHoldings());
    }

    public String printMoney() {
        return (isHuman()) ? ("You have " + money) : (name + " has: " + money);
    }

    public String serialize() {
        String str = "{";
        str += "\"gameId\":" + gameId;
        str += ",\"name\": \"" + name + "\"";
        str += ",\"playerNum\":" + playerNum;
        str += ",\"money\":" + money;
        str += ",\"holdings\":" + holdings.serialize();
        str += ",\"tiles\":[";
        for (int i = 0; i < tiles.size(); i++) {
            str += tiles.get(i).serialize();
            if (i < tiles.size() - 1) {
                str += ",";
            }

        }
        str += "]";
        str += "}";
        return str;
    }

    /**
     *
     * @param targetType the type we're getting rid of, to get the bidder type
     * @param bankHoldings how many of the bidder type are left in the bank
     * @return the largest number of the target shares we can trade in for
     * bidder shares.
     */
    public Integer getMaxTradableHoldings(StockType targetType, Integer bankHoldings) {
        //The number we can trade is limited by whichever is lower, the amount the bank has of the bidder stock and the amount we have of the target stock
        //and we can only trade an even number of shares
        return Math.min(bankHoldings * 2,//If the bank has 2, I can trade up to 4
                ((holdings.get(targetType) % 2 == 0) //if the number I have is even, I can trade up to all of them
                 ? holdings.get(targetType)
                 : holdings.get(targetType) - 1)); //otherwise, I can trade up to all - 1
    }

    /**
     * Doesn't remove the tile from the player's tiles list yet, since we don't
     * know if it's even a valid play yet. releaseTile removes it.
     *
     * @return a tile from the player's hand.
     */
    public abstract TileCoords selectTile();

    /**
     * It might be getting discarded or it might be getting played, but in any
     * case it's leaving our hand.
     *
     * @param tile
     */
    public void releaseTile(TileCoords tile) {
        tiles.remove(tile);
    }

    public abstract Sale buyStocks(Holdings availableStocks, Map<StockType, Chain> activeChains);

    public abstract StockType chooseNewChainType(List<StockType> availableChains, Holdings holdingsRemaining);

    public abstract StockType chooseWinningChain(List<StockType> tiedChains);

    public abstract Integer promptTrade(StockType targetType, StockType bidderType, Integer bidderBankShares);

    public abstract Integer promptSell(StockType type, Integer price);

    public abstract boolean isHuman();
}
