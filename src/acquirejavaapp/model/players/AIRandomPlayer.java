package acquirejavaapp.model.players;

import acquirejavaapp.model.Chain;
import acquirejavaapp.model.Holdings;
import acquirejavaapp.model.Sale;
import acquirejavaapp.model.StockType;
import acquirejavaapp.model.TileCoords;
import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AIRandomPlayer extends Player {

    @Expose(serialize = false)
    private final Random rand;

    public AIRandomPlayer(Integer gameId, String name, Integer playerNum) {
        super(gameId, name, playerNum);
        rand = new Random();
    }

    public AIRandomPlayer(Integer gameId, String name, Integer playerNum, Integer money, Holdings holdings) {
        super(gameId, name, playerNum, money, holdings);
        rand = new Random();
    }

    @Override
    public TileCoords selectTile() {
        return tiles.get(rand.nextInt(tiles.size()));
    }

    @Override
    public Sale buyStocks(Holdings availableStocks, Map<StockType, Chain> activeChains) {
        Holdings itemsToBuy = new Holdings();
        List<Chain> affordableChains = new ArrayList<>();
        Integer totalPrice = 0;
        Integer tempMoney = money;
        for (int i = 0; i < 3; i++) {
            affordableChains.clear();
            for (Chain chain : activeChains.values()) {
                if (chain.getPrice() <= tempMoney) {
                    affordableChains.add(chain);
                }
            }
            if (affordableChains.isEmpty()) {
                break;
            }
            Chain item = affordableChains.get(rand.nextInt(affordableChains.size()));
            if (availableStocks.has(item.getType(), itemsToBuy.get(item.getType()) + 1) && item.getPrice() <= tempMoney) {
                itemsToBuy.increment(item.getType(), 1);
                totalPrice += item.getPrice();
                tempMoney = tempMoney - item.getPrice();
            }
        }
        return new Sale(itemsToBuy, totalPrice);
    }

    @Override
    public StockType chooseNewChainType(List<StockType> availableChains, Holdings holdingsRemaining) {
        return (availableChains.get(rand.nextInt(availableChains.size())));
    }

    @Override
    public StockType chooseWinningChain(List<StockType> tiedChains) {
        return (tiedChains.get(rand.nextInt(tiedChains.size())));
    }

    @Override
    public Integer promptTrade(StockType targetType, StockType bidderType, Integer bidderBankShares) {
        Integer max = getMaxTradableHoldings(targetType, bidderBankShares);
        Integer tradeCount = 1;
        while (tradeCount % 2 == 0) {
            //nextInt gives a value from zero inclusive to the input exclusive. We want to include max.
            tradeCount = rand.nextInt(max + 1);
        }
        return tradeCount;
    }

    @Override
    public Integer promptSell(StockType type, Integer price) {
        Integer max = holdings.get(type);
        //nextInt gives a value from zero inclusive to the input exclusive. We want to include max.
        Integer tradeCount = rand.nextInt(max + 1);
        return tradeCount;
    }

    @Override
    public boolean isHuman() {
        return false;
    }
}
