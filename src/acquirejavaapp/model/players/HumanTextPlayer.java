package acquirejavaapp.model.players;

import acquirejavaapp.model.Board;
import acquirejavaapp.model.Chain;
import acquirejavaapp.model.Holdings;
import acquirejavaapp.model.Sale;
import acquirejavaapp.model.StockType;
import acquirejavaapp.model.TileCoords;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class HumanTextPlayer extends Player {

    private final Scanner scan;

    public HumanTextPlayer(Integer gameId, String name, Integer playerNum, Integer money, Holdings holdings, Scanner scan) {
        super(gameId, name, playerNum, money, holdings);
        this.scan = scan;
    }

    public HumanTextPlayer(Integer gameId, String name, int playerNum, Scanner scan) {
        super(gameId, name, playerNum);
        this.scan = scan;
    }

    @Override
    public TileCoords selectTile() {
        System.out.println(printTiles());
        System.out.println("   1     2     3     4     5     6");
        Integer tileIndex = -1;
        while (tileIndex == -1) {
            System.out.print("\n" + getName() + ", play a tile:");
            String tileInput = scan.nextLine();
            if (tileInput.isEmpty()) {
                tileIndex = -1;
                continue;
            }
            try {
                tileIndex = Integer.parseInt(tileInput);

            }
            catch (NumberFormatException e) {
            }
            if (tileIndex == -1) {
                tileIndex = getTileFromText(tileInput);
            }
            //if we've gotten the tile and it's still -1, we don't have what you wanted.
            if (tileIndex == -1) {
                System.out.print("You do not have that tile.");
            }
        }

        TileCoords chosenTile = tiles.get(tileIndex - 1);
        return chosenTile;
    }

    private Integer getTileFromText(String tileInput) {
        TileCoords tile;
        Integer tileIndex = -1;
        try {
            tile = new TileCoords(tileInput);
            for (int i = 0; i < tiles.size(); i++) {
                if (tiles.get(i).equals(tile)) {
                    tileIndex = i + 1;
                }
            }
        }
        catch (ParseException e) {
            System.err.println("couldn't get a tile from that input.");
            System.err.println("please try again.");
        }
        return tileIndex;
    }

    @Override
    public Sale buyStocks(Holdings bankHoldings, Map<StockType, Chain> activeChains) {
        System.out.println(name + ", " + printHoldings());
        System.out.println("This is what is up for sale:");
        System.out.println(bankHoldings.printHoldings(activeChains.values()));
        System.out.println(printMoney());
        System.out.println(name + ", what stocks will you buy?");
        Holdings stocksToBuy = new Holdings();
        String input = null;
        Integer sum = 0;
        while (input == null || input.length() > 3) {
            System.out.print("Enter the first letter of each stock you want:");
            input = scan.nextLine().replace(" ", "");
            if (input.length() > 3) {
                System.out.println("please only enter 3 letters.");
                continue;
            }
            else if (input.length() == 0) {
                return new Sale(new Holdings(), 0);
            }
            //important to zero these out after bad input.
            sum = 0;
            stocksToBuy = new Holdings();
            for (int i = 0; i < input.length(); i++) {
                StockType type = StockType.getStockTypeFromChar(input.charAt(i));
                stocksToBuy.increment(type, 1);
                if (type == null) {
                    System.out.println("Couldn't parse " + input.charAt(i) + " as a type of stock.");
                    input = null;
                    break;
                }
                if (activeChains.get(type) == null || !bankHoldings.has(type, stocksToBuy.get(type))) {
                    System.out.println(stocksToBuy.get(type) + " " + type + " is not available.");
                    input = null;
                    break;
                }
                sum += activeChains.get(type).getPrice();
                if (sum > money) {
                    System.out.println("You cannot afford that option.");
                    input = null;
                    break;
                }
            }
        }
        return new Sale(stocksToBuy, sum);
    }

    @Override
    public StockType chooseNewChainType(List<StockType> availableChains, Holdings holdingsRemaining) {
        System.out.println("Which hotel chain would you like to start?");
        String out = "";
        for (StockType chain : availableChains) {
            Integer cardsLeft = holdingsRemaining.get(chain);
            out += chain.name() + ((cardsLeft != Board.STOCK_COUNT) ? ("(" + cardsLeft + " left)") : "");
            out += "\n";
        }
        System.out.println(out);

        return pickStock(availableChains);
    }

    private StockType pickStock(List<StockType> availableChains) {
        StockType choice = null;
        while (choice == null) {
            System.out.print("pick a stock (first letter for short):");
            String input = scan.nextLine() + " ";
            choice = StockType.getStockTypeFromChar(input.charAt(0));
            if (!availableChains.contains(choice)) {
                choice = null;
            }
        }
        return choice;
    }

    @Override
    public StockType chooseWinningChain(List<StockType> tiedChains) {
        System.out.println("Which chain should take over the other" + ((tiedChains.size() > 2) ? "s" : "") + "?");
        for (int i = 0; i < tiedChains.size(); i++) {
            System.out.println(tiedChains.get(i));
        }
        return pickStock(tiedChains);
    }

    /**
     *
     * @param targetType
     * @param bidderType
     * @param bidderBankShares
     * @return the number of bidderType stock the player will receive. (2x the
     * number of targetType stock they will lose.)
     */
    @Override
    public Integer promptTrade(StockType targetType, StockType bidderType, Integer bidderBankShares) {
        System.out.println(name + ", how many of your " + holdings.get(targetType) + " " + targetType + " stock "
                           + "would you like to trade two-for-one for " + bidderType + " stock?");
        Integer maxInput = getMaxTradableHoldings(targetType, bidderBankShares);
        Integer input = null;
        while (input == null || input < 0 || input > maxInput || input % 2 != 0) {
            //maxTradable is the number of the bidderType stock remaining
            // so 2x that is the max the player could trade in.
            System.out.print("Enter an even number from 0-" + maxInput + ":");
            try {
                input = scan.nextInt();
                scan.nextLine();
            }
            catch (StringIndexOutOfBoundsException | NumberFormatException e) {
                System.out.println("bad input.");
                input = null;
            }
        }
        return input / 2;
    }

    @Override
    public Integer promptSell(StockType type, Integer price) {
        System.out.println("how many of your " + type + " stock would you like to sell for " + price + " each?");

        Integer input = null;
        while (input == null || input < 0 || input > holdings.get(type)) {
            System.out.print("Enter a number from 0-" + holdings.get(type) + ":");
            try {
                input = scan.nextInt();
                scan.nextLine();
            }
            catch (StringIndexOutOfBoundsException | NumberFormatException e) {
                System.out.println("bad input.");
                input = null;
            }
        }
        return input;
    }

    @Override
    public boolean isHuman() {
        return true;
    }
}
