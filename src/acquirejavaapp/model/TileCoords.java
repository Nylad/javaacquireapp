package acquirejavaapp.model;

import com.google.gson.annotations.Expose;
import java.text.ParseException;

public class TileCoords implements Comparable<TileCoords> {

    @Expose
    private final int row;
    @Expose
    private final int col;
    private final static String[] ROWS = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};

    public TileCoords(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public TileCoords(String tileString) throws ParseException {
        tileString = tileString.trim();
        String rowText = "" + tileString.charAt(tileString.length() - 1);
        int rowIndex = -1;
        for (int i = 0; i < ROWS.length; i++) {
            if (ROWS[i].equals(rowText.toUpperCase())) {
                rowIndex = i;
            }
        }
        if (rowIndex == -1) {
            throw new ParseException("Couldn't parse tile row from value", tileString.length() - 1);
        }
        this.row = rowIndex;
        //actual column number is 1 less than display column number.
        this.col = Integer.parseInt(tileString.substring(0, tileString.length() - 1)) - 1;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public String toString() {
        String spacer = "" + ((col + 1 < 10) ? " " : "");
        return spacer + " " + (col + 1) + ROWS[row] + " ";
    }

    /**
     * player who draws a tile with the lowest number starts the game.
     *
     * @param target
     * @return negative if this tile is lower (better) than the target tile, 0
     * if they're the same tile (wat) or positive if this tile is higher than
     * the target tile.
     */
    @Override
    public int compareTo(TileCoords target) {
        if (this.col == target.getCol()) {
            return Integer.compare(this.row, target.getRow());
        }
        else {
            return Integer.compare(this.col, target.getCol());
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.row;
        hash = 79 * hash + this.col;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TileCoords other = (TileCoords) obj;
        if (this.row != other.row) {
            return false;
        }
        else if (this.col != other.col) {
            return false;
        }
        return true;
    }

    public String serialize() {
        String str = "{                     \"row\": " + row + ",\n"
                     + "                    \"col\": " + col + ",\n}";
        return str;
    }
}
