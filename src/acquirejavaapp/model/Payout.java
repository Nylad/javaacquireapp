package acquirejavaapp.model;

public class Payout {

    private final StockType targetType;
    private final StockType bidderType;
    private final int price;

    public Payout(StockType targetType, StockType bidderType, int price) {
        this.targetType = targetType;
        this.bidderType = bidderType;
        this.price = price;
    }

    public StockType getTargetType() {
        return targetType;
    }

    public StockType getBidderType() {
        return bidderType;
    }

    public int getPrice() {
        return price;
    }

}
