package acquirejavaapp.model;

import com.google.gson.annotations.Expose;
import java.text.ParseException;

public class Tile implements Comparable<Tile> {

    @Expose
    private final TileCoords coords;
    @Expose
    private boolean drawn;
    @Expose
    private boolean placed;
    @Expose
    private boolean discarded;
    private Chain chain;
    @Expose
    private StockType type;

    public Tile(int row, int col) {
        this.coords = new TileCoords(row, col);
        this.drawn = false;
        this.placed = false;
        this.chain = null;
        this.discarded = false;
        this.type = null;
    }

    /**
     * Serialiazation constructor.
     *
     * @param row
     * @param col
     * @param drawn
     * @param placed
     * @param discarded
     * @param chainId
     * @param type
     */
    public Tile(int row, int col, Boolean drawn, Boolean placed, Boolean discarded, Integer chainId, StockType type) {
        this.coords = new TileCoords(row, col);
        this.drawn = drawn;
        this.placed = placed;
        this.discarded = discarded;
        this.chain = null;
        this.type = type;
    }

    /**
     * this entire constructor is ridiculous. It's only for text inputs.
     *
     * @param tileString
     * @throws java.text.ParseException
     */
    public Tile(String tileString) throws ParseException {
        this.coords = new TileCoords(tileString);
        this.drawn = false;
        this.placed = false;
        this.chain = null;
        this.discarded = false;
        this.type = null;
    }

    public void setDrawn() {
        drawn = true;
    }

    public Boolean isDrawn() {
        return drawn;
    }

    public void setDiscarded() {
        discarded = true;
    }

    public Boolean isDiscarded() {
        return discarded;
    }

    public void setPlaced() {
        placed = true;
    }

    public Boolean isPlaced() {
        return placed;
    }

    public int getRow() {
        return coords.getRow();
    }

    public int getCol() {
        return coords.getCol();
    }

    @Override
    public String toString() {
        return coords.toString();
    }

    public String getDisplayValue() {
        if (isPlaced()) {
            if (isPartOfChain() && chain.getType() != null) {
                return StockType.getSymbol(chain.getType());
            }
            else if (isDiscarded()) {
                return "     ";
            }
            else {
                return "XXXXX";
            }
        }
        return coords.toString();
    }

    public String serialize() {
        String str = "{                     \"row\": " + coords.getRow() + ",\n"
                     + "                    \"col\": " + coords.getCol() + ",\n"
                     + "                    \"drawn\": " + drawn + ",\n"
                     + "                    \"placed\": " + placed + ",\n"
                     + "                    \"discarded\": " + discarded + "}";
        return str;
    }

    /**
     * player who draws a tile with the lowest number starts the game.
     *
     * @param target
     * @return negative if this tile is lower (better) than the target tile, 0
     * if they're the same tile (wat) or positive if this tile is higher than
     * the target tile.
     */
    @Override
    public int compareTo(Tile target) {
        return this.coords.compareTo(target.coords);
    }

    public void setChain(Chain chain) {
        this.chain = chain;
        this.type = chain.getType();
    }

    public StockType getType() {
        return type;
    }

    public Chain getChain() {
        return chain;
    }

    public boolean isPartOfChain() {
        return chain != null;
    }

}
