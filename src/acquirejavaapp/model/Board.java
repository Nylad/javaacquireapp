package acquirejavaapp.model;

import acquirejavaapp.model.bag.Bag;
import acquirejavaapp.model.bag.RandomBag;
import acquirejavaapp.model.players.Player;
import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Board {

    @Expose
    private List<Chain> inactiveChains;
    @Expose
    private Map<StockType, Chain> activeChains;
    @Expose
    private final Tile[][] tiles; //1A through 12I. Row is letter, column is number.
    @Expose
    private final Holdings holdings;
    private final Bag bag;

    public static final int ROWS = 9;
    public static final int COLUMNS = 12;
    public static final int STOCK_COUNT = 25;

    public Board(Holdings holdings, Tile[][] tiles, List<Chain> chains) {
        this.holdings = holdings;
        this.tiles = tiles;
        augmentBoardWithChains(chains);
        List<TileCoords> remainingTiles = new ArrayList<>();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (!tiles[i][j].isDrawn()) {
                    remainingTiles.add(new TileCoords(i, j));
                }
            }
        }
        this.bag = new RandomBag(remainingTiles);
    }

    public Board() {
        inactiveChains = new ArrayList<>();
        activeChains = new HashMap<>();
        tiles = createStartingTiles();
        this.holdings = new Holdings(25, 25, 25, 25, 25, 25, 25);
        this.bag = new RandomBag();
    }

    public String serialize() {
        String json = "{ \"tiles\": [";
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                json += tiles[i][j].serialize();
                if (j < tiles[i].length - 1) {
                    json += ",";
                }
            }
            if (i < tiles.length - 1) {
                json += ",";
            }
        }
        json += "],";
        json += "\"holdings\": ";
        json += holdings.serialize();
        json += "}";
        return json;
    }

    /**
     * each tile knows which chain it's associated with in the map, so we just
     * have to switch from referencing the ID to referencing the actual chain
     * and add the tiles to the correct chain's list.
     *
     * @param chains
     */
    private void augmentBoardWithChains(List<Chain> chains) {
        for (Chain chain : chains) {
            for (Tile tile : chain.getTiles()) {
                tiles[tile.getRow()][tile.getCol()].setChain(chain);
                chain.addTile(tile);
                /**
                 * TODO: this has problems with duplicating our tiles,
                 * basically. We get one out of storage and then link one from
                 * the real board's tiles. Could fix by deduplicating them here,
                 * or could fix better by making tiles be simple coordinate
                 * pairs when passed around and have the board be the one to
                 * know stuff about the tiles themselves. tiles are where.
                 */
            }
        }

    }

    public static Tile[][] createStartingTiles() {
        Tile[][] tempTiles = new Tile[ROWS][COLUMNS];
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                tempTiles[i][j] = new Tile(i, j);
            }
        }
        return tempTiles;
    }

    public Tile[][] getTiles() {
        return tiles;
    }

    /**
     * gets the tile specified by the bag. This way the bag doesn't have to know
     * the board.
     *
     * @return
     */
    public TileCoords getTileFromBag() {
        TileCoords tileCoords = bag.getTileFromBag();
        if (tileCoords == null) {
            return null;
        }
        return tileCoords;
    }

    /**
     *
     * @return a list of all chains on the board. Includes initiative null
     * chains as well as loner null chains.
     */
    public List<Chain> getInactiveChains() {
        return inactiveChains;
    }

    /**
     *
     * @return a map of each possible stock type to its current corresponding
     * chain on the board.
     */
    public Map<StockType, Chain> getActiveChains() {
        return activeChains;
    }

    /**
     *
     * @return a list of all stocks not currently in play, thus available to
     * open.
     */
    public List<StockType> getInactiveStocks() {
        List<StockType> inactiveStocks = new ArrayList<>();
        for (StockType stock : StockType.values()) {
            if (getActiveChains().get(stock) == null) {
                inactiveStocks.add(stock);
            }
        }
        return inactiveStocks;
    }

    public Holdings getHoldings() {
        return holdings;
    }

    /**
     * Places an active tile. //TODO: refactor all these to take coordinates,
     * not tiles. The caller shouldn't need a reference to the board's array of
     * tiles.
     *
     * @param player
     * @param tile
     * @return list of stocks and their prices at the time of the merger
     */
    public List<Payout> placeActiveTile(Player player, TileCoords tile) {
        return placeTile(player, tiles[tile.getRow()][tile.getCol()], true);//TODO: switch to tileCoord?
    }

    /**
     * Places a tile with lower priority, so that initiative chains get taken
     * over instead of take new tiles over.
     *
     * @param player
     * @param tile
     */
    public void placeInitiativeTile(Player player, TileCoords tile) {
        placeTile(player, tiles[tile.getRow()][tile.getCol()], false);
    }

    /**
     * if a tile has adjacent tiles and none of the adjacent tiles are part of
     * an active chain, then this tile would start a new chain.
     *
     * @param tile tile under consideration.
     * @return true if placing this tile would start a new stock chain.
     */
    public boolean wouldStartNewChain(TileCoords tile) {
        List<Chain> adjacents = getAdjacentChains(tile);
        return !adjacents.isEmpty() && adjacents.stream().allMatch(ch -> !ch.isActive());
    }

    /**
     * if a tile has more than one adjacent chain of size 11 or higher, this
     * tile can't be played.
     *
     * @param tile
     * @return true if this tile is between two or more "safe" chains (of size
     * 11 or higher)
     */
    public boolean wouldMergeSafeChains(TileCoords tile) {
        List<Chain> adjacents = getAdjacentChains(tile);
        return !adjacents.isEmpty() && adjacents.stream().filter(ch -> ch.size() > 10).count() > 1;
    }

    private List<Payout> placeTile(Player player, Tile tile, Boolean active) {
        tile.setPlaced();
        player.releaseTile(new TileCoords(tile.getRow(), tile.getCol()));
        Chain newTileChain = new Chain(null, active);
        tile.setChain(newTileChain);
        newTileChain.addTile(tile);
        List<Chain> mergingChains = new ArrayList<>();
        mergingChains.addAll(getAdjacentChains(new TileCoords(tile.getRow(), tile.getCol())));

        if (mergingChains.isEmpty()) {
            //if this is a loner tile it's inactive and can now be taken over
            newTileChain.setActive(false);
            inactiveChains.add(newTileChain);
            return Collections.emptyList();
        }
        mergingChains.add(newTileChain);
        mergingChains.sort(Comparator.reverseOrder());
//        //if the winner is inactive, we must be merging initiative chains. That
//        //means no payouts and size doesn't matter.
//        if (!mergingChains.get(0).isActive()) {
//            Chain newNullChain = new Chain(null, false);
//            for (Chain mergingChain : mergingChains) {
//                newNullChain.addTiles(mergingChain.getTiles());
//            }
//            return new ArrayList<>();
//        }
        //two active chains of the same size, have to ask player which to keep
        if (mergingChains.get(0).compareTo(mergingChains.get(1)) == 0
            && mergingChains.get(0).isActive()) {
            mergingChains = resolveTiedChains(player, mergingChains);
        }
        Chain winner = mergingChains.get(0);
        Chain loser;
        List<Payout> payouts = new ArrayList<>();
        while (mergingChains.size() > 1) {
            winner = mergingChains.get(0);
            loser = mergingChains.remove(1);
            inactiveChains.remove(loser);
            //the newly placed tile will be active and be one of these chains, and we don't want payouts for it. So check that type isn't null.
            if (loser.isActive() && loser.getType() != null) {
                payouts.add(new Payout(loser.getType(), winner.getType(), loser.getPrice()));
                activeChains.remove(loser.getType());
            }
            winner.assimilate(loser);
        }
        //starting new chain
        if (winner.getType() == null && winner.isActive()) {
            winner.setType(player.chooseNewChainType(getInactiveStocks(), holdings));
            activeChains.put(winner.getType(), winner);
            giveStockToPlayer(winner.getType(), player);//don't forget the free stock!
        }
        return payouts;
    }

    /**
     * present the player with the stock type of all chains tied for largest,
     * and the chain with the type they pick is put at the top.
     *
     * @param player
     * @param mergingChains a size-sorted list (desc) of the potential chains
     * that could win this tie
     * @return a reordered list of the merging chains, so that the player's
     * choice takes over the others.
     */
    private List<Chain> resolveTiedChains(Player player, List<Chain> mergingChains) {
        List<StockType> tiedChainTypes = new ArrayList<>();
        tiedChainTypes.add(mergingChains.get(0).getType());
        // Start at 1. Could be up to a 4-way tie, but we know the first is an option.
        for (int i = 1; i < mergingChains.size(); i++) {
            if (mergingChains.get(i).compareTo(mergingChains.get(0)) == 0) {
                tiedChainTypes.add(mergingChains.get(i).getType());
            }
        }
        StockType winnerType = player.chooseWinningChain(tiedChainTypes);

        for (int i = 0; i < mergingChains.size(); i++) {
            //important to .equals in this order, because the null chain of the
            //tile being placed is also in this list of chains.
            if (winnerType.equals(mergingChains.get(i).getType())) {
                Chain temp = mergingChains.remove(i);
                mergingChains.add(0, temp);
            }
        }
        return mergingChains;
    }

    public void giveStockToPlayer(StockType type, Player player) {
        sellStockToPlayer(type, 1, player, 0);
    }

    public void sellStockToPlayer(StockType type, Integer count, Player player, Integer price) {
        holdings.decrement(type, count);
        player.getHoldings().increment(type, count);
        player.setMoney(player.getMoney() - (price * count));
    }

    public void sellStocksToPlayer(Sale sale, Player player) {
        holdings.subtractAllHoldings(sale.getPurchases());
        player.getHoldings().addAllHoldings(sale.getPurchases());
        player.setMoney(player.getMoney() - sale.getTotalPrice());
    }

    public void buyStockFromPlayer(StockType type, Integer count, Player player, Integer price) {
        holdings.increment(type, count);
        player.getHoldings().decrement(type, count);
        player.setMoney(player.getMoney() + (price * count));
    }

    public void tradeStockWithPlayer(StockType bidderType, StockType targetType, Integer bidderTypeCount, Player player) {
        holdings.decrement(bidderType, bidderTypeCount);
        holdings.increment(targetType, bidderTypeCount * 2);
        player.getHoldings().increment(bidderType, bidderTypeCount);
        player.getHoldings().decrement(targetType, bidderTypeCount * 2);
    }

    public void giveBonusToPlayer(Player player, Integer amount) {
        player.setMoney(player.getMoney() + amount);
    }

    public List<Chain> getAdjacentChains(TileCoords tile) {
        Set<Chain> adjacentChains = new HashSet<>();
        if (tile.getRow() > 0) {
            Tile north = tiles[tile.getRow() - 1][tile.getCol()];
            if (north.isPartOfChain()) {
                adjacentChains.add(north.getChain());
            }
        }
        if (tile.getRow() < ROWS - 1) {
            Tile south = tiles[tile.getRow() + 1][tile.getCol()];
            if (south.isPartOfChain()) {
                adjacentChains.add(south.getChain());
            }
        }
        if (tile.getCol() > 0) {
            Tile west = tiles[tile.getRow()][tile.getCol() - 1];
            if (west.isPartOfChain()) {
                adjacentChains.add(west.getChain());
            }
        }
        if (tile.getCol() < COLUMNS - 1) {
            Tile east = tiles[tile.getRow()][tile.getCol() + 1];
            if (east.isPartOfChain()) {
                adjacentChains.add(east.getChain());
            }
        }
        List chainList = Arrays.asList(adjacentChains.toArray());//todo: this is not the best
        return chainList;
    }

    public Map<StockType, Integer> getPrices() {
        Map<StockType, Integer> prices = new HashMap<>();
        for (StockType stock : StockType.values()) {
            Chain chain = activeChains.get(stock);
            if (chain != null) {
                prices.put(stock, chain.getPrice());
            }
        }
        return prices;
    }

    public String printBoard() {
        String out = "                                                                         \n";
        for (int i = 0; i < tiles.length; i++) {
            out += " ";
            for (int j = 0; j < tiles[0].length; j++) {
                Tile tile = tiles[i][j];
                out += tile.getDisplayValue() + " ";
            }
            out += "\n";
            out += "                                                                         \n";

        }
        return out;
    }

    public String printBoard(List<TileCoords> highlightedTileCoords) {

        //List<Pair<Integer, Integer>> coords = highlightedTiles.stream().map((tile) -> new Pair<>(tile.getRow(), tile.getCol())).collect(toList());
        String out = getBorderRowHighlighted(-1, highlightedTileCoords);
        for (int i = 0; i < tiles.length; i++) {
            out += getBorderColumnHighlighted(i, -1, highlightedTileCoords);
            for (int j = 0; j < tiles[0].length; j++) {
                Tile tile = tiles[i][j];
                out += tile.getDisplayValue() + getBorderColumnHighlighted(i, j, highlightedTileCoords);
            }
            out += "\n";
            out += getBorderRowHighlighted(i, highlightedTileCoords);

        }
        return out;
    }

    private String getBorderRowHighlighted(int rowNumber, List<TileCoords> coords) {
        String rowText = "";
        if (rowNumber == -1) {
            for (int j = 0; j < COLUMNS; j++) {
                if (coords.contains(new TileCoords(rowNumber + 1, j))) {
                    rowText += " -----";
                }
                else {
                    rowText += "      ";
                }
            }
        }
        else if (rowNumber > -1 && rowNumber < ROWS) {
            for (int j = 0; j < COLUMNS; j++) {
                if (coords.contains(new TileCoords(rowNumber, j)) || coords.contains(new TileCoords(rowNumber + 1, j))) {
                    rowText += " -----";
                }
                else {
                    rowText += "      ";
                }
            }
        }
        else {
            for (int j = 0; j < COLUMNS; j++) {
                if (coords.contains(new TileCoords(rowNumber, j))) {
                    rowText += " -----";
                }
                else {
                    rowText += "      ";
                }
            }
        }
        return rowText + " \n";
    }

    private String getBorderColumnHighlighted(int rowNumber, int colNumber, List<TileCoords> coords) {
        if (colNumber == -1) {
            if (coords.contains(new TileCoords(rowNumber, colNumber + 1))) {
                return "|";
            }
        }
        else if (colNumber > -1 && colNumber < COLUMNS) {
            if (coords.contains(new TileCoords(rowNumber, colNumber)) || coords.contains(new TileCoords(rowNumber, colNumber + 1))) {
                return "|";
            }
        }
        else if (coords.contains(new TileCoords(rowNumber, colNumber))) {
            return "|";
        }
        return " ";
    }

    public String printPlayerStatus(Player player) {
        if (player.isHuman()) {
            String out = player.printHoldings();
            out += player.printMoney() + "\n";
            out += printBoard(player.getTiles());
            return out;
        }
        else {
            return "";
        }
    }

    /**
     *
     * @param coords
     */
    public void discardTile(TileCoords coords) {
        Tile tile = tiles[coords.getRow()][coords.getCol()];
        tile.setPlaced();
        tile.setDiscarded();
    }

}
