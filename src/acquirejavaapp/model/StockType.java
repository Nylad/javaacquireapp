package acquirejavaapp.model;

public enum StockType {
    TOWER,
    LUXOR,
    AMERICAN,
    WORLDWIDE,
    FESTIVAL,
    IMPERIAL,
    CONTINENTAL;

    public static String getSymbol(StockType stock) {
        switch (stock) {
            case TOWER:
                return " ~T~ ";
            case LUXOR:
                return "][L][";
            case AMERICAN:
                return "__A__";
            case WORLDWIDE:
                return "\\\\W\\\\";
            case FESTIVAL:
                return "/ F /";
            case IMPERIAL:
                return ">>I>>";
            case CONTINENTAL:
                return "< C <";
            default:
                return "";
        }
    }

    public static StockType getStockTypeFromChar(char input) {
        StockType choice;
        switch ((input + "").toUpperCase().charAt(0)) {
            case 'A':
                choice = StockType.AMERICAN;
                break;
            case 'F':
                choice = StockType.FESTIVAL;
                break;
            case 'T':
                choice = StockType.TOWER;
                break;
            case 'I':
                choice = StockType.IMPERIAL;
                break;
            case 'W':
                choice = StockType.WORLDWIDE;
                break;
            case 'C':
                choice = StockType.CONTINENTAL;
                break;
            case 'L':
                choice = StockType.LUXOR;
                break;
            default:
                choice = null;
        }
        return choice;
    }
}
