package acquirejavaapp.model.bag;

import static acquirejavaapp.model.Board.COLUMNS;
import static acquirejavaapp.model.Board.ROWS;
import acquirejavaapp.model.TileCoords;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nylad
 */
public abstract class Bag {

    protected static List<TileCoords> tiles;

    public Bag() {
        tiles = new ArrayList<>();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                tiles.add(new TileCoords(i, j));
            }
        }
    }

    /**
     * deserialization constructor.
     *
     * @param remainingTiles
     */
    public Bag(List<TileCoords> remainingTiles) {
        tiles = remainingTiles;
    }

    /**
     *
     * @return a pair of ints representing the coordinates of a tile that hasn't
     * been drawn yet, or null if there are no more tiles to draw. Shouldn't
     * ever give the same tile twice.
     */
    public abstract TileCoords getTileFromBag();
}
