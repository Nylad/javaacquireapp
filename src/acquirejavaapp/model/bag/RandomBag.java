package acquirejavaapp.model.bag;

import acquirejavaapp.model.TileCoords;
import java.util.List;
import java.util.Random;

public class RandomBag extends Bag {

    private final Random rand;

    public RandomBag() {
        super();
        this.rand = new Random();
    }

    public RandomBag(List<TileCoords> tilesRemaining) {
        super(tilesRemaining);
        this.rand = new Random();
    }

    @Override
    public TileCoords getTileFromBag() {
        if (tiles.isEmpty()) {
            return null;
        }
        TileCoords tile;
        tile = tiles.remove(rand.nextInt(tiles.size()));
        return tile;
    }
}
