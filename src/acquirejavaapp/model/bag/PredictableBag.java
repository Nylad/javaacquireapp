package acquirejavaapp.model.bag;

import acquirejavaapp.model.TileCoords;

public class PredictableBag extends Bag {

    private Integer current;

    public PredictableBag() {
        super();
        //TODO: give more interesting order of tiles than just row by row
        //tiles = new ArrayList<Pair<Integer,Integer>>();
        this.current = 0;
    }

    @Override
    public TileCoords getTileFromBag() {
        if (current > tiles.size() - 1) {
            return null;
        }
        else {
            return tiles.get(current++);
        }
    }

}
