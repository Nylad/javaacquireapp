package acquirejavaapp.model.bag;

import acquirejavaapp.model.TileCoords;
import java.text.ParseException;
import java.util.Scanner;

public class TextInputBag extends Bag {

    private final Scanner scan;

    public TextInputBag(Scanner scan) {
        this.scan = scan;
    }

    @Override
    public TileCoords getTileFromBag() {
        System.out.print("Pick a tile that hasn't been drawn yet (empty for random):");
        TileCoords textTile = null;
        TileCoords tile = null;
        while (textTile == null) {
            try {
                String tileString = scan.nextLine();
                if (tileString.isEmpty()) {
                    return tiles.remove(0);
                }
                textTile = new TileCoords(tileString);
                tile = new TileCoords(textTile.getRow(), textTile.getCol());
                if (tiles.contains(tile)) {
                    System.out.println("that tile has been drawn. Please choose another.");
                    textTile = null;
                    tile = null;
                }
            }
            catch (StringIndexOutOfBoundsException | ParseException | NumberFormatException e) {
                System.err.println("Bad input! " + e);
                System.out.print("try again:");
            }
        }
        System.out.println("drew tile " + textTile.toString());
        return tile;
    }

}
