package acquirejavaapp.model;

import com.google.gson.annotations.Expose;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Holdings {

    @Expose
    private final Map<StockType, Integer> holdings;

    /**
     * setup a holdings object with no holdings.
     */
    public Holdings() {
        this(0, 0, 0, 0, 0, 0, 0);
    }

    public Holdings(int tower, int luxor, int american, int worldwide, int festival, int imperial, int continental) {
        holdings = new HashMap<>();
        holdings.put(StockType.TOWER, tower);
        holdings.put(StockType.LUXOR, luxor);
        holdings.put(StockType.AMERICAN, american);
        holdings.put(StockType.WORLDWIDE, worldwide);
        holdings.put(StockType.FESTIVAL, festival);
        holdings.put(StockType.IMPERIAL, imperial);
        holdings.put(StockType.CONTINENTAL, continental);
    }

    public Integer get(StockType type) {
        return holdings.get(type);
    }

    public void set(StockType type, Integer value) {
        holdings.put(type, value);
    }

    public String printHoldings() {
        String out = "";
        for (StockType stock : StockType.values()) {
            if (get(stock) > 0) {
                out += "\t" + stock.name() + ": " + get(stock) + "  ";
            }
        }
        return (out.isEmpty() ? "\tNone\n" : out);
    }

    public void addAllHoldings(Holdings addedHoldings) {
        holdings.entrySet().forEach((entry) -> {
            increment(entry.getKey(), addedHoldings.get(entry.getKey()));
        });
    }

    public boolean hasAllHoldings(Holdings other) {
        boolean hasAll = true;
        for (StockType type : StockType.values()) {
            if (!has(type, other.get(type))) {
                hasAll = false;
            }
        }
        return hasAll;
    }

    public void subtractAllHoldings(Holdings subtractedHoldings) {
        holdings.entrySet().forEach((entry) -> {
            decrement(entry.getKey(), subtractedHoldings.get(entry.getKey()));
        });
    }

    public String printHoldings(Collection<Chain> chains) {
        String out = "";
        Integer typeCount = 0;
        for (Chain chain : chains) {
            StockType type = chain.getType();
            if (get(type) > 0) {
                out += "\t" + type + " (size " + chain.size() + "): " + get(type) + " @" + chain.getPrice() + " each\n";
                typeCount++;
            }
        }
        //making our output be the same number of lines every time.
        while (typeCount < StockType.values().length - 1) {
            out += "\n";
            typeCount++;
        }
        return out;
    }

    public void increment(StockType chain, Integer value) {
        holdings.put(chain, value + holdings.get(chain));
    }

    public boolean has(StockType chain, Integer value) {
        return holdings.get(chain) >= value;
    }

    public void decrement(StockType chain, Integer value) {
        holdings.put(chain, holdings.get(chain) - value);
    }

    public boolean isEmpty() {
        int sum = 0;
        for (Integer value : holdings.values()) {
            sum += value;
        }
        return sum == 0;
    }

    public Iterable<Map.Entry<StockType, Integer>> entrySet() {
        return holdings.entrySet();
    }

    public String serialize() {
        String str = "                 {\"tower\": " + holdings.get(StockType.TOWER) + ",\n"
                     + "                \"luxor\": " + holdings.get(StockType.LUXOR) + ",\n"
                     + "                \"american\": " + holdings.get(StockType.AMERICAN) + ",\n"
                     + "                \"worldwide\": " + holdings.get(StockType.WORLDWIDE) + ",\n"
                     + "                \"festival\": " + holdings.get(StockType.FESTIVAL) + ",\n"
                     + "                \"imperial\": " + holdings.get(StockType.IMPERIAL) + ",\n"
                     + "                \"continental\": " + holdings.get(StockType.CONTINENTAL) + "}";
        return str;
    }

}
