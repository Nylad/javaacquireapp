package acquirejavaapp.model;

import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Every placed, non-discarded tile has a chain associated with it, even single
 * tiles. A chain is activated when a player places a tile next to an inactive
 * tile or chain of tiles.
 */
public class Chain implements Comparable<Chain> {

    @Expose
    private StockType type;

    @Expose
    private final List<Tile> tiles;
    @Expose
    private Boolean active;

    public Chain(StockType type) {
        this.tiles = new ArrayList<>();
        this.active = true;
        this.type = type;
    }

    public Chain(StockType type, Boolean active) {
        this.tiles = new ArrayList<>();
        this.active = active;
        this.type = type;
    }

    public StockType getType() {
        return type;
    }

    public void setType(StockType type) {
        this.type = type;
    }

    public List<Tile> getTiles() {
        return tiles;
    }

    public void addTiles(List<Tile> newTiles) {
        newTiles.forEach(t -> t.setChain(this));
        tiles.addAll(newTiles);
    }

    public void addTile(Tile newTile) {
        newTile.setChain(this);
        tiles.add(newTile);
    }

    public Integer size() {
        return tiles.size();
    }

    public Integer getPrice() {
        switch (type) {
            case TOWER:
            case LUXOR:
            default:
                return getBasePrice();
            case AMERICAN:
            case WORLDWIDE:
            case FESTIVAL:
                return (getBasePrice()) + 100;
            case IMPERIAL:
            case CONTINENTAL:
                return (getBasePrice()) + 200;
        }
    }

    private int getBasePrice() {
        if (size() <= 5) {
            return size() * 100;
        }
        if (size() <= 10) {
            return 600;
        }
        else if (size() <= 20) {
            return 700;
        }
        else if (size() <= 30) {
            return 800;
        }
        else if (size() <= 40) {
            return 900;
        }
        else {
            return 1000;
        }
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.type);
        hash = 79 * hash + Objects.hashCode(this.tiles);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Chain other = (Chain) obj;
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.tiles, other.tiles)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Chain otherChain) {
        if (this.active && !otherChain.active) {
            return 1;
        }
        else if (!this.active && otherChain.active) {
            return -1;
        }
        return Integer.compare(this.size(), otherChain.size());
    }

    public void assimilate(Chain target) {
        addTiles(target.getTiles());
    }

}
