package acquirejavaapp.dao;

import java.util.HashMap;
import java.util.Map;

public class InMemoryDao {

    private final Map<Integer, String> gameStates;

    public InMemoryDao() {
        this.gameStates = new HashMap<>();
    }

    public String getState(Integer gameId) {
        return gameStates.get(gameId);
    }

    public void saveState(Integer gameId, String gameState) {
        gameStates.put(gameId, gameState);
        System.out.println(gameStates.get(gameId));
    }
}
