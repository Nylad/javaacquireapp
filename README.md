﻿
# Command-Line Acquire Board Game

Fully-playable implementation of the board game [Acquire](https://en.wikipedia.org/wiki/Acquire), for pass-and-play human players, AI players, or any combination of the two. The full rules of the game can be found [here.](https://www.ultraboardgames.com/acquire/game-rules.php)


# Setup
Clone the project:
> `git clone https://Nylad@bitbucket.org/Nylad/javaacquireapp.git`

## Usage
In a command prompt or shell, navigate to the "dist" folder within the project.
> `cd path/to/project/javaacquireapp/dist`

Assuming java is on your PATH variable, run the application as a jar:
> `java -jar AcquireJavaApp.jar`

The application will guide you through starting a game, prompting you for how many players will play and what to name them. 

To start a game immediately instead, use the following format of arguments:
> `java -jar AcquireJavaApp.jar number_of_players p1_name p2_name [p3_name] ...`

where the number of space-delimited names entered must match the number of players specified. 
Names may be appended with "[AI]" to specify a computer player, otherwise they will be a pass-and-play human player.
For example, this: 
> `java -jar AcquireJavaApp.jar 5 Jack[AI] Jill[AI] Hansel[AI] Gretel[AI] Player`

will create a game with 4 AI players and a human player named "Player."

